Requirements:
1. Registration Page 
   - A Registration Page with proper form validation (all fields must be filled and passwords must match) 
   that must redirect the user to the login page once successfully submitted.
2. Login Page
   - A Login Page that must redirect the user to the either the home page or the products catalog 
   once successfully authenticated
3. Product Catalog  
   - A Product Catalog page that must show all products currently available (unavailable/inactive products must not be shown) 
   and allow users to visit individual product pages
4. Specific Product Page
   - A Specific Product page that must show all relevant product information, with a feature to allow only logged-in users to 
   add any quantity of the product to their cart
5. Cart View
   - A Cart View page with the following features:
      - Show all items the user has added to their cart (and their quantities)
      - Change product quantities
      - Remove products from cart
      - Subtotal for each item
      - Total price for all items
      - A working checkout button/functionality.
      - When the user checks their cart out, redirect them to either the homepage or the Order History page
6. Order Page
   - An Order History page where the currently logged-in user can see all records of their own previously-placed orders
7. Admin/Dashboard
   - An Admin Panel/Dashboard with the following features:
      - Retrieve list of all products (available or unavailable)
      - Create new product
      - Update product information
      - Deactivate/reactivate product
8. Other Requirements:
    - A fully-functioning Navbar with proper dynamic rendering (Register/Login links for users not logged in, 
    Logout link for users who are, etc)
    - App must be single-page and utilize proper routing (no navigating to another page/reloading)
    - Registration/Login pages must be inaccessible to users who are logged-in
    - Apart from users who are not logged-in, Admin must not be able to add products to their cart
    - Do not create a website other than the required e-commerce app
    - Do not use templates found in other sites

Stretch Goals:
  - Full responsiveness across mobile/tablet/desktop screen sizes
  - Product images
  - A hot products/featured products section
  - Admin feature to retrieve a list of all orders made by all users

House Rules
 - 1 Lifeline per student in each group
 - Project Managers will create their Google Meet link and send it to the Batch Group Chat
 - Make sure to return to class by no later than 9:15 PM PHT
 - Push your current progress to Gitlab and paste the link in Boodle before dismissal
